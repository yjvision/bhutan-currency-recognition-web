from django.shortcuts import render
from django.core.files.storage import FileSystemStorage
from json import loads
import tensorflow as tf
# from tensorflow import Graph, Session
from keras.models import load_model
from keras.preprocessing import image
import numpy as np



# Create your views here.
def index(request):
	context = {
		'a':1
	}
	return render(request, 'index.html', context)

def aboutProject(request):
	context = {}

	return render(request, 'about_project.html', context)

def team(request):
	context = {}

	return render(request, 'team.html', context)


# Define parameters
img_height, img_width = 224, 224
with open('./models/classes.json', 'r') as f:
	labelInfo = f.read()

labelInfo = loads(labelInfo)

# model_graph = Graph()
# with model_graph.as_default():
# 	tf_session = Session()
# 	with tf_session.as_default():
model = load_model('./models/currency_model.h5')


def predictImage(request):
	if request.method == "POST":
		# Get the name of the uploaded file
		fileObj = request.FILES['filePath']
		# Create an object of FileSystemStorage
		fs = FileSystemStorage()
		# Save the file
		filePathName = fs.save(fileObj.name, fileObj)
		filePathName = fs.url(filePathName)
		testimage = '.'+filePathName

		# CLASSIFICATION
		img = image.load_img(testimage, target_size=(img_height, img_width))
		x = image.img_to_array(img)
		x = x / 255
		x = x.reshape(1,img_height, img_width,3)
		predict = model.predict(x)
		predictedLabel = labelInfo[str(np.argmax(predict[0]))]
		
		context = {
			'filePathName':filePathName,
			'predictedLabel':predictedLabel
		}

		return render(request, "index.html", context)