from os import name
from django.urls import path
from . import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('', views.index, name='index'),
	path('about/', views.aboutProject, name='about'),
	path('team/', views.team, name='team'),
	path('predictImage', views.predictImage, name='predictImage')
]
urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)