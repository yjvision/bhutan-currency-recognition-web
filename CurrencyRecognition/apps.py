from django.apps import AppConfig


class CurrencyrecognitionConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'CurrencyRecognition'
